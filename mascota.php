<?php
include("conexion.php");




function generarCodigo($longitud) {
 $key = '';
 $pattern = '123456789';
 $max = strlen($pattern)-1;
 for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
 return $key;
}
 

?>

<!DOCTYPE HTML>
<html lang="en">

<head>
  <title>Nueva moscota </title>
  <link rel="stylesheet" href="estilossss.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
  

  <div class="encabezado">  
          <h1>Nueva Mascota</h1>
  </div>

    
<form   action="ingresarmascota.php" class="was-validated" enctype="multipart/form-data" method="POST"  type="hidden"  >
  <div class="contenedor3">
    <div class="form-row">
    
      <div class="col-md-4">
        <label for="cod_mascota">codigo de la mascota:</label>
       <input type="number" value="<?php echo generarCodigo(6) ?>"  name="cod_mascota"  id= "cod_mascota"  readonly>
       
      </div>
        

      <div class="col-md-4">
        <label for="nombre">Nombre de la mascota:</label>
        <input type="text" class="form-control" id="nombre_m" placeholder="Ingrese nombre " name="nombre_m" required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>
   
      <div class="col-md-6">
        <label for="edad">edad:</label>
        <input type="number" class="form-control" id="edad" placeholder="Ingrese la edad" name="edad"min="1" pattern="^[0-9]+ " required>
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>

         <div class="col-md-3">
        <label for="rut_dueño">Rut dueño de la mascota:</label>
        <input type="text" class="form-control" required oninput="checkRut(this)" id="rut_usuario" placeholder="Ingrese rut" name="rut_usuario" required >
        <div class="valid-feedback">Correcto</div>
        <div class="invalid-feedback">Rellene este campo</div>
      </div>


<option selected value="0"> Eliga el sexo </option>
               <select id="sexo" name="sexo">
  <option value="macho">Macho  </option>
  <option value="hembra">Hembra</option>
 
</select>


<option selected value="0"> Eliga la especie</option>
               <select id="especie" name="especie">
  <option value="perro">Perro</option>
  <option value="gato">Gato</option>
 <option value="pajaro">Pajaro</option>
  <option value="tortuja">Tortuja</option>
  <option value="pez">Pez</option>
  <option value="roedores">Roedores</option>
</select> 



    </div>

    

   
   <button   type="submit" class="btn btn-success" style=" margin:0 auto;" >Ingresar</button>
    <input type="button" class="btn btn-danger" value="Cancelar" onclick="history.back() "/>
</div>

</form>


 <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script type="text/javascript">
function checkRut(rut) {
    // Despejar Puntos
    var valor = rut.value.replace('.','');
    // Despejar Guión
    valor = valor.replace('-','');
    
    // Aislar Cuerpo y Dígito Verificador
    cuerpo = valor.slice(0,-1);
    dv = valor.slice(-1).toUpperCase();
    
    // Formatear RUN
    rut.value = cuerpo + '-'+ dv
    
    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if(cuerpo.length < 7 || cuerpo.length > 8) { rut.setCustomValidity("Rut no valido"); return false;}
    
    // Calcular Dígito Verificador
    suma = 0;
    multiplo = 2;
    
    // Para cada dígito del Cuerpo
    for(i=1;i<=cuerpo.length;i++) {
    
        // Obtener su Producto con el Múltiplo Correspondiente
        index = multiplo * valor.charAt(cuerpo.length - i);
        
        // Sumar al Contador General
        suma = suma + index;
        
        // Consolidar Múltiplo dentro del rango [2,7]
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
  
    }
    
    // Calcular Dígito Verificador en base al Módulo 11
    dvEsperado = 11 - (suma % 11);
    
    // Casos Especiales (0 y K)
    dv = (dv == 'K')?10:dv;
    dv = (dv == 0)?11:dv;
    
    // Validar que el Cuerpo coincide con su Dígito Verificador
    if(dvEsperado != dv) { rut.setCustomValidity("RUT no existe"); return false; }
    
    // Si todo sale bien, eliminar errores (decretar que es válido)
    rut.setCustomValidity('');
}

</script>
  
</body>
</html>